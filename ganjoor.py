import os

import requests
import tweepy
from apscheduler.schedulers.blocking import BlockingScheduler

api_key = os.getenv("API_KEY")
api_key_secret = os.getenv("API_KEY_SECRET")
access_token = os.getenv("ACCESS_TOKEN")
access_token_secret = os.getenv("ACCESS_TOKEN_SECRET")


def send_a_poem():
    response = requests.post("http://c.ganjoor.net/beyt-json.php")
    m1 = response.json()["m1"]
    m2 = response.json()["m2"]
    poet = response.json()["poet"]
    url = response.json()["url"]
    if m1 and m2 and poet:
        try:
            auth = tweepy.OAuthHandler(api_key, api_key_secret)
            auth.set_access_token(access_token, access_token_secret)
            # Redirect user to Twitter to authorize
            # auth_url = auth.get_authorization_url()
            # print(auth_url)
            # verifier_value = input("Enter verifier value")
            # # Get access token
            # auth.get_access_token(verifier_value)
            api = tweepy.API(auth)
            poem = m1 + "\n" + m2 + "\n\n" + poet
            api.update_status(poem)
            print(response.json())
        except Exception as e:
            print("error in sending tweet => ", e.args)


scheduler = BlockingScheduler()
# hours=1
# minutes=2
# seconds=10
scheduler.add_job(send_a_poem, 'interval', hours=int(os.getenv("SCHEDULER_HOURS", 1)))
scheduler.start()
